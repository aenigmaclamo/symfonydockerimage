FROM php:5.6-apache
MAINTAINER Kevin Raoofi <nullindev@gmail.com>

ADD config/php.ini /usr/local/etc/php/
ADD config/000-symfonysite.conf /etc/apache2/sites-available/
RUN ln -s /etc/apache2/sites-available/000-symfonysite.conf /etc/apache2/sites-enabled/
 
RUN apt-get update -qq
RUN apt-get install -y git
RUN apt-get update && apt-get install -y zlib1g-dev \
    && docker-php-ext-install zip

# Install composer
RUN curl -sS https://getcomposer.org/installer | php \
 && mv composer.phar /usr/local/bin/composer

# Install the symfony installer, as well
RUN curl -LsS http://symfony.com/installer -o /usr/local/bin/symfony
RUN chmod a+x /usr/local/bin/symfony

# Cleaning up
RUN apt-get clean && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*

# Port for Development
EXPOSE 8000
